# Crystal Design System Demo

![Crystal Design System](./mockup-image.png)

This is a demo project to show how to use the Crystal Design System in order to build out an angular application.

## This demo covers:

1. Referencing a Crystal Figma mockup, Analysing the design and determining the components needed.
2. Starting from a set up project, and building a page component (focused on template and styles only).
3. Building layouts with grid, flexbox and Utility classes provided by Crystal Toolkit.
4. Using Crystal Icons, Crystal Angular and Crystal Toolkit to build out the components.
5. Extending and creating custom styles to add missing functionality (noting which could be contributed back to Crystal).

## Mockup Link:

[QC Data Issues Tab Mockup](https://www.figma.com/design/kAd4SErfXb4Ywztq4IoCs2/Demo-Example-Page?node-id=0-1&t=tDR3qQg2bahTe6EY-1)

## Video Link:

[Link To live Demo Recording](https://clarioclinical-my.sharepoint.com/:v:/g/personal/kevin_calderwood_clario_com/EeuM-ISRfIdOiBkmLaljVZgBL1Ds4pAK-mwf8Rnk74z27g?e=GwitFB)
