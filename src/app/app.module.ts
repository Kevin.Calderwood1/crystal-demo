import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { QcDataIssuesPageComponent } from "./features/qc-data-issues-page/qc-data-issues-page.component";

@NgModule({
    declarations: [AppComponent, QcDataIssuesPageComponent],
    imports: [BrowserModule, AppRoutingModule],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
