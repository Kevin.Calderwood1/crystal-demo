import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { QcDataIssuesPageComponent } from "./features/qc-data-issues-page/qc-data-issues-page.component";

const routes: Routes = [{ path: "", component: QcDataIssuesPageComponent }];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
