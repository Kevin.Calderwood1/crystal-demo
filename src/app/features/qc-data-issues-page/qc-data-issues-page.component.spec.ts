import { ComponentFixture, TestBed } from "@angular/core/testing";

import { QcDataIssuesPageComponent } from "./qc-data-issues-page.component";

describe("QcDataIssuesPageComponent", () => {
    let component: QcDataIssuesPageComponent;
    let fixture: ComponentFixture<QcDataIssuesPageComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [QcDataIssuesPageComponent],
        });
        fixture = TestBed.createComponent(QcDataIssuesPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });
});
