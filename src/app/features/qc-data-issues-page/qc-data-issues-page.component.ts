import { ChangeDetectionStrategy, Component } from "@angular/core";
@Component({
    selector: "app-qc-data-issues-page",
    templateUrl: "./qc-data-issues-page.component.html",
    styleUrls: ["./qc-data-issues-page.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QcDataIssuesPageComponent {}
